//dotenv
require('dotenv').config();

const database = process.env.DATABASE;
const port = process.env.PORT;

//import fastify
const fastify = require('fastify')({
  logger: true,
});

//bring in routes
const routes = require('./routes');

//db
const mongoose = require('mongoose');
mongoose
  .connect(database, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('mongo is ready!!'))
  .catch((err) => console.log(err));

//mongodb+srv://kempsmm:kemps@1234@bootcamp-fullstack.ldefx.mongodb.net/fastify-crash-course?retryWrites=true&w=majority

//routes
fastify.get('/', (request, reply) => {
  return { visiter: 'LearCode.in' };
});

routes.forEach((route, index) => {
  fastify.route(route);
});

//starting server
const start = async () => {
  try {
    await fastify.listen(port || 3000);
    fastify.log.info(`Server is running at ${address}`);
  } catch (error) {}
};

start();
